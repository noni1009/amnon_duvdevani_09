#include "functions.h"
#include <iostream>
#include <string>
#include "MyClass.h"

using namespace std;

int main() {

	//check compare
	cout << "correct print is 1 -1 0" << endl;
	cout << compare<double>(1.0, 2.5) << endl;
	cout << compare<double>(4.5, 2.4) << endl;
	cout << compare<double>(4.4, 4.4) << endl;

	//check bubbleSort
	cout << "correct print is sorted array" << endl;

	const int arr_size = 5;
	double doubleArr[arr_size] = { 1000.0, 2.0, 3.4, 17.0, 50.0 };
	bubbleSort<double>(doubleArr, arr_size);
	for (int i = 0; i < arr_size; i++) {
		cout << doubleArr[i] << " ";
	}
	cout << endl;

	//check printArray
	cout << "correct print is sorted array" << endl;
	printArray<double>(doubleArr, arr_size);
	cout << endl;

	// ------------- CHAR CHECKINGS ---------------------------
	char charArr[arr_size] = { 'a', 'c', 'd', 'b', 'z' };
	bubbleSort<char>(charArr, arr_size);
	for (int i = 0; i < arr_size; i++) {
		cout << charArr[i] << " ";
	}
	cout << endl;

	//check printArray
	cout << "correct print is sorted array" << endl;
	printArray<char>(charArr, arr_size);
	cout << endl;


	MyClass *vars = new MyClass();
	vars->num = 5;

	MyClass *vars2 = new MyClass();
	vars2->num = 10;


	cout << vars << endl;
	cout << vars2 << endl;

	cout << (vars < vars2) << endl;

	vars2 = vars;
	cout << vars2 << endl;


	system("pause");
	return 1;
}