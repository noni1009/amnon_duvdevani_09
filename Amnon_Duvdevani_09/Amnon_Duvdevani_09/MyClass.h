#pragma once
#include <stdio.h>
#include <iostream>

using namespace std;

class MyClass
{
public:
	int num;

	bool operator<(const MyClass other);
	MyClass& operator=(const MyClass other);
	friend ostream& operator<< (ostream &o, const MyClass &c);
};