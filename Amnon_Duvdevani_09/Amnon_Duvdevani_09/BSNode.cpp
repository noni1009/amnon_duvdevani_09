#include "BSNode.h"
#include <iostream>

BSNode::BSNode(string data)
{
	_data = data;
	_right = nullptr;
	_left = nullptr;
	_count = 1;
}


BSNode::BSNode(const BSNode& other)
{
	BSNode node = other;
}


BSNode::~BSNode()
{
	if (_left != nullptr)
	{
		_left->~BSNode();
	}
	
	if (_right != nullptr)
	{
		_right->~BSNode();
	}
}

void BSNode::insert(string value)
{
	// if value to insert is equal to current node value, return
	if (value == _data)
	{
		_count++;
		return;
	}

	// checks if value to insert is smaller then current node value
	if (value < _data)
	{
		if (_left == nullptr)	// if no smaller node already exists, create new one
		{
			_left = new BSNode(value); // create new node and point from _left
		}
		else
		{
			_left->insert(value); // check next node
		}
	}

	// checks if value to insert is bigger then current node value
	else if (value > _data)
	{
		if (_right == nullptr)	// if no bigger node already exists, create new one
		{
			_right = new BSNode(value); // create new node and point from _right
		}
		else
		{
			_right->insert(value); // check next node
		}
	}
}

bool BSNode::search(string val) const
{
	bool found = false;	// init variable to return

	// if value to search is equal to current node value, return
	if (val == _data)
	{
		return true;
	}

	// if no childs return false 
	if (this->isLeaf())
	{
		return false;
	}

	// checks if value to search is smaller then current node value
	if (val < _data)
	{
		if (_left != nullptr)	
		{
			found = _left->search(val);	// RECURRSIVE search again
		}
	}

	// checks if value to insert is bigger then current node value
	else if (val > _data)
	{
		if (_right != nullptr)	
		{
			found = _right->search(val); // RECURRSIVE search again
		}
	}

	return found;
}

// check if node is leaf
bool BSNode::isLeaf() const
{
	if (_right == nullptr && _left == nullptr)	// if dosent have left and dosent have right, is leaf
	{
		return true;
	}
	else
	{
		return false;
	}
}

int BSNode::getHeight() const
{
	if (this == nullptr)
		return 0;

	else
	{
		/* compute the depth of each subtree */
		int lDepth = this->getLeft()->getHeight();
		int rDepth = this->getRight()->getHeight();

		/* use the larger one */
		if (lDepth > rDepth)
			return(lDepth + 1);
		else return(rDepth + 1);
	}
}

int BSNode::getDepth(const BSNode& root) const
{
	int rootHeight = root.getHeight();	// gets height of root
	
	if (!root.search(this->getData()))	// if the root given isn't a root of the current object
		return -1;
	
	int nodeHeight = this->getHeight();	// gets height of given current object

	return(rootHeight - nodeHeight);	// depth of object equals to root height - object height
}

void BSNode::printNodes() const
{
	if (this->isLeaf())	// if leaf, print
	{
		cout << this->getData() << " " << this->getCount() << endl;
	}
	else
	{
		if (this->getLeft() != nullptr)	// if there are smallers, then print them
		{
			this->getLeft()->printNodes();
		}		
		else                            // if no smallers or all smallers already printed, print
		{
			cout << this->getData() << " " << this->getCount() << endl;
		}

		//	if has left and right then print
		if (this->getLeft() != nullptr && this->getRight() != nullptr)
		{
			cout << this->getData() << " " << this->getCount() << endl;
		}

		// after all smallers have been printed, now print biggers. 
		// if there are biggers then print them
		if (this->getRight() != nullptr)
		{
			this->getRight()->printNodes();
		}
		// if no biggers or all biggers already printed, print
		else
		{
			cout << this->getData() << " " << this->getCount() << endl;
		}
	}
}

int BSNode::getCount() const
{
	return _count;
}

string BSNode::getData() const
{
	return _data;
}

BSNode* BSNode::getLeft() const
{
	return _left;
}

BSNode* BSNode::getRight() const
{
	return _right;
}


// ----------------------------------- OPERATOR'S ------------------
BSNode& BSNode::operator=(const BSNode& other)
{
	BSNode* node = new BSNode(other.getData());	// create new node with data of correct "other" node

	if (other.getLeft() != nullptr)	// if left has value in it
	{
		node->_left = other.getLeft();	// RECURRSIVE -> create new child 
	}
	
	if (other.getRight() != nullptr) // if right has value in it
	{
		node->_right = other.getRight(); // RECURRSIVE -> create new child 
	}

	// return new node
	return *node;
}