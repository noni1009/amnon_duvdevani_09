#include "MyClass.h"
#include <iostream>

bool MyClass::operator<(const MyClass other)
{
	if (num < other.num)
		return true;

	return false;
}

MyClass& MyClass::operator=(const MyClass other)
{
	MyClass *temp = new MyClass();

	temp->num = other.num;
	
	return *temp;
}

ostream& operator<< (ostream &o, const MyClass &c)
{
	o << c.num;
	return o;
}

