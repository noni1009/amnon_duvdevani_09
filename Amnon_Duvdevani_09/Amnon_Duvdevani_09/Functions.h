#pragma once

template <class T>
int compare(T var1, T var2)
{
	if (var1 < var2)
		return 1;

	else if (var1 == var2)
		return 0;
	
	return -1;
}


template <class T>
void bubbleSort(T *arr, int size)
{
	int i = 0;
	int j = 0;
	T temp;

	for (i = 1; i < size; i++)
	{
		for (j = size - 1; j >= i; j--)
		{
			if(compare(arr[j - 1], arr[j]) == -1)
			{
				temp = arr[j - 1];
				arr[j - 1] = arr[j];
				arr[j] = temp;
			}
		}
	}
}

template <class T>
void printArray(T *arr, int size)
{
	for (int i = 0; i < size; i++)
	{
		cout << arr[i] << endl;
	}
}

